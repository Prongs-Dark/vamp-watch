import { Injectable } from '@angular/core';
import { ethers } from "ethers";
import { FANTOM_CHAIN_DETAILS, SUPPORTED_CHAINS } from '../utils/constants';
import { Notification } from '../utils/notification';
import { ChainNotAddedNotification } from '../utils/notifications/chain-not-added-notification';
import { NoProviderNotification } from '../utils/notifications/no-provider-notification';
import { SuccessfullConnectionNotification } from '../utils/notifications/successfull-connection-notification';

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  public web3: ethers.providers.Web3Provider|null;
  public wallet: string = '';

  constructor() { 
    if ((window as any).ethereum) {
      this.web3 = new ethers.providers.Web3Provider((window as any).ethereum);
    }
    else {
      this.web3 = null;
    }
  }

  public async connect(): Promise<Notification> {
    if (!this.web3) {
      return NoProviderNotification;
    }
    else {
      await this.web3.send("eth_requestAccounts", []);
      let address = await this.web3.getSigner().getAddress();

      const chainId = (await this.web3.getNetwork()).chainId;
      
      return this.checkAndSwitchChain(chainId)
      .then(() => {
        this.wallet = address;
        return SuccessfullConnectionNotification;
      })
      .catch((reason: Notification) => {
        return reason;
      });
    }
  }

  private async checkAndSwitchChain(chainId: number): Promise<void> {
    if (!SUPPORTED_CHAINS.includes(chainId)) {
      try {
        await this.web3?.send('wallet_switchEthereumChain', [{ chainId: FANTOM_CHAIN_DETAILS.chainId }]);
        return Promise.resolve();
      }
      catch(switchError: any) {
        if (switchError.code === 4902 || switchError.code === -32603) {
          try {
            await this.web3?.send('wallet_addEthereumChain', [FANTOM_CHAIN_DETAILS])
            return Promise.resolve();
          }
          catch (_) {
            return Promise.reject(ChainNotAddedNotification)
          }
        }
        else {
          return Promise.reject(ChainNotAddedNotification)
        }
      }
    }
    else {
      return Promise.resolve();
    }
  }
}
