import { Notification, NotificationType } from "../notification";

export const SuccessfullConnectionNotification: Notification = new Notification(NotificationType.ERROR, "Wallet connected successfully.", 1);