import { Notification, NotificationType } from "../notification";

export const ChainNotAddedNotification: Notification = new Notification(NotificationType.ERROR, "Chain was not added to your wallet", -2);