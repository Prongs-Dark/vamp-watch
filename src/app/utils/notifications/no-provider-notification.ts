import { Notification, NotificationType } from "../notification";

export const NoProviderNotification: Notification = new Notification(NotificationType.ERROR, "No web3 provider was found. Use a web3-enabled browser.", -2);