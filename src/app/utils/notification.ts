export class Notification {
    public type: NotificationType;
    public message: string;
    public code?: number;

    constructor(type: NotificationType, message: string, code?: number) {
        this.type = type;
        this.message = message;
        this.code = code;
    }
}

export enum NotificationType {
    ERROR,
    WARNING,
    SUCCESS,
    INFO
}