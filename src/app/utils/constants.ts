export const FANTOM_CHAIN_DETAILS = {
    "chainId": '0xfa',
    "chainName": 'Fantom Opera',
    "nativeCurrency": {
        name: 'Fantom',
        symbol: 'FTM',
        decimals: 18
    },
    "rpcUrls": ['https://rpc.ftm.tools/'],
    "blockExplorerUrls": ['https://ftmscan.com'],
};

export const SUPPORTED_CHAINS: Array<number> = new Array(250);