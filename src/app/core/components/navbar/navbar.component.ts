import { Component, OnInit } from '@angular/core';
import { faWallet, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { WalletService } from 'src/app/services/wallet.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public faWallet: IconDefinition = faWallet
  public wallet: string = '';

  constructor(private walletService: WalletService) { }

  ngOnInit(): void {
  }

  /**
   * Connects to the web3 provider through the wallet service
   */
  public async connectWallet() {
    await this.walletService.connect();
    this.wallet = this.walletService.wallet;
  }

}
